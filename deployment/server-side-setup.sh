#!/bin/bash
# Install NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

# Install Node
nvm install node

# Install pm2
npm install pm2 -g

# Install ffmpeg
sudo apt install -y ffmpeg

mkdir bot
tar -zxvf bot.tar.gz -C bot/
mv DEPLOY_ENV_FILE bot/.env

cd bot
npm install --production
pm2 start index.js