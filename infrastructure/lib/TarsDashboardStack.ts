import { DnsValidatedCertificate } from "@aws-cdk/aws-certificatemanager";
import { CloudFrontWebDistribution } from "@aws-cdk/aws-cloudfront";
import { ARecord, RecordTarget } from "@aws-cdk/aws-route53";
import { CloudFrontTarget } from "@aws-cdk/aws-route53-targets";
import { Bucket } from "@aws-cdk/aws-s3";
import { BucketDeployment, Source } from "@aws-cdk/aws-s3-deployment";
import { Construct, RemovalPolicy, Stack, StackProps } from "@aws-cdk/core";
import { TarsPreexistingResourcesStack } from "./common/TarsPreexistingResourcesStack"
// import { Runtime, Code, Function } from "@aws-cdk/aws-lambda";
// import { LambdaRestApi } from "@aws-cdk/aws-apigateway";

export class TarsDashboardStack extends Stack {
  constructor(
    scope: Construct,
    id: string,
    preExistingResourcesStack: TarsPreexistingResourcesStack,
    props?: StackProps
  ) {
    super(scope, id, props);
    // const handler = new Function(this, "TarsSoundUploadHandler", {
    //   runtime: Runtime.NODEJS_12_X,
    //   code: Code.fromAsset("lambda/soundUpload.js"),
    //   handler: "soundUpload.generateUrl",
    //   environment: {
    //     BUCKET: supportStack.bucket.bucketName,
    //   },
    // });
    // supportStack.bucket.grantReadWrite(handler);

    // const api = new LambdaRestApi(this, "TarsLambdaApi", {
    //   handler,
    // });

    const siteBucket = new Bucket(this, "TarsDashboardBucket", {
      publicReadAccess: true,
      removalPolicy: RemovalPolicy.DESTROY,
      websiteIndexDocument: "index.html",
    });

    const siteBucketDeployment = new BucketDeployment(
      this,
      "TarsDashboardDeployment",
      {
        sources: [Source.asset("../dashboard/public/")],
        destinationBucket: siteBucket,
        // distributionPaths: ["*"],
      }
    );

    const certificate = new DnsValidatedCertificate(
      this,
      "TarsDashboardCertificate",
      {
        domainName: preExistingResourcesStack.domainName,
        hostedZone: preExistingResourcesStack.hostedZone,
        region: "us-east-1", // need this for auto validation, WHY??!!??
      }
    );

    const siteDistribution = new CloudFrontWebDistribution(this, "CloudFront", {
      aliasConfiguration: {
        acmCertRef: certificate.certificateArn,
        names: [preExistingResourcesStack.domainName],
      },
      originConfigs: [
        {
          s3OriginSource: {
            s3BucketSource: siteBucket,
          },
          behaviors: [{ isDefaultBehavior: true }],
        },
      ],
    });

    const aRecord = new ARecord(this, "TarsDashboardARecord", {
      zone: preExistingResourcesStack.hostedZone,
      target: RecordTarget.fromAlias(new CloudFrontTarget(siteDistribution)),
    });
  }
}
