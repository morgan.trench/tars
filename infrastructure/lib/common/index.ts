import { TarsPreexistingResourcesStack } from "./TarsPreexistingResourcesStack";
import { TarsSupportingStack } from "./TarsSupportingStack";

export {
    TarsPreexistingResourcesStack,
    TarsSupportingStack
}