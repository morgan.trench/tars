import { HostedZone, IHostedZone } from "@aws-cdk/aws-route53";
import { Construct, Stack, StackProps } from "@aws-cdk/core";

export class TarsPreexistingResourcesStack extends Stack {
  domainName: string;
  hostedZone: IHostedZone;

  constructor(
    scope: Construct,
    id: string,
    props: StackProps,
    preExistingResourceDetails: any
  ) {
    super(scope, id, props);
    this.domainName = preExistingResourceDetails.domainName;
    this.hostedZone = HostedZone.fromLookup(
      this,
      preExistingResourceDetails.hostedZoneId,
      {
        domainName: preExistingResourceDetails.domainName,
        privateZone: false,
      }
    );
  }
}
