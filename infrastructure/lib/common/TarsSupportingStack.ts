import {
  CfnEIP,
  Peer,
  Port,
  SecurityGroup,
  SubnetType,
  Vpc,
} from "@aws-cdk/aws-ec2";
import { Bucket } from "@aws-cdk/aws-s3";
import { Construct, Stack, StackProps } from "@aws-cdk/core";
export class TarsSupportingStack extends Stack {
  vpc: Vpc;
  securityGroup: SecurityGroup;
  eip: CfnEIP;
  bucket: Bucket;

  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    this.vpc = new Vpc(this, "TarsVPC", {
      natGateways: 0,
      subnetConfiguration: [
        { name: "TarsVpcPublicSubnet Subnet", subnetType: SubnetType.PUBLIC },
      ],
    });
    this.securityGroup = new SecurityGroup(this, "TarsSecurityGroup", {
      vpc: this.vpc,
    });
    this.securityGroup.addIngressRule(
      Peer.anyIpv4(),
      Port.tcp(22),
      "SSH from anywhere"
    );
    this.securityGroup.addIngressRule(
      Peer.anyIpv4(),
      Port.tcp(443),
      "HTTPS (Text Chat)"
    );
    this.securityGroup.addIngressRule(
      Peer.anyIpv4(),
      Port.udpRange(50000, 65535),
      "UDP Range (Voice)"
    );

    this.eip = new CfnEIP(this, "TarsElasticIp");

    this.bucket = new Bucket(this, "TarsResourcesBucket", {});
  }
}
