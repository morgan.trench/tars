import {
  CfnEIPAssociation,
  Instance,
  InstanceType,
  MachineImage,
  SubnetType,
  UserData,
} from "@aws-cdk/aws-ec2";
import { CfnOutput, Construct, Stack, StackProps } from "@aws-cdk/core";
import { TarsSupportingStack } from "./common/TarsSupportingStack";

export class TarsInstanceStack extends Stack {
  constructor(
    scope: Construct,
    id: string,
    supportStack: TarsSupportingStack,
    props?: StackProps
  ) {
    super(scope, id, props);

    const ubuntuImage = MachineImage.lookup({
      name: "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20200408",
    });

    const userData = UserData.forLinux();
    userData.addCommands("apt-get update && apt-get -y upgrade");

    const ec2Instance = new Instance(this, "TarsInstance", {
      vpc: supportStack.vpc,
      instanceType: new InstanceType("t2.nano"),
      machineImage: ubuntuImage,
      keyName: "TarsDeployment",
      securityGroup: supportStack.securityGroup,
      vpcSubnets: { subnetType: SubnetType.PUBLIC },
      userData,
    });
    supportStack.bucket.grantRead(ec2Instance);

    new CfnEIPAssociation(this, "TarsElasticIpAssociation", {
      eip: supportStack.eip.ref,
      instanceId: ec2Instance.instanceId,
    });

    new CfnOutput(this, "TarsInstanceBucket", {
      value: supportStack.bucket.bucketName,
    });

    new CfnOutput(this, "TarsInstancePublicIp", {
      value: ec2Instance.instancePublicIp,
    });
  }
}
