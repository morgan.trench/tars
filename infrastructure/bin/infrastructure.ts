#!/usr/bin/env node
import "source-map-support/register";
import * as cdk from "@aws-cdk/core";
import {
  TarsPreexistingResourcesStack,
  TarsSupportingStack,
} from "../lib/common";
import { TarsInstanceStack } from "../lib/TarsInstanceStack";
import { TarsDashboardStack } from "../lib/TarsDashboardStack";
import { StackProps } from "@aws-cdk/core";

const app = new cdk.App();
const commonProps: StackProps = {
  env: {
    region: "ap-southeast-2",
    account: "978649538140",
  },
};
const preExistingResourceDetails = {
  hostedZoneId: "Z0022604B00UPOO7I57T",
  domainName: "tarsbot.xyz",
};

const preExsitngResourceStack = new TarsPreexistingResourcesStack(
  app,
  "TarsPreExistingResources",
  commonProps,
  preExistingResourceDetails
);
const supportStack = new TarsSupportingStack(app, "TarsSuppport", commonProps);

const dashboardStack = new TarsDashboardStack(
  app,
  "TarsDashboard",
  preExsitngResourceStack,
  commonProps
);
dashboardStack.addDependency(preExsitngResourceStack);

const instanceStack = new TarsInstanceStack(
  app,
  "TarsInstance",
  supportStack,
  commonProps
);
instanceStack.addDependency(supportStack);
