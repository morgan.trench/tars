import { config } from './config';
import { Client } from 'discord.js';
import { Greetings } from './functions/greetings/greetings';
import { SoundBoard } from './functions/soundboard';

// Initialize
const client = new Client();
[Greetings, SoundBoard].forEach((x) => x(client));

// Housekeeping behaviour
client.on('ready', () => {
    console.log(`Logged in as ${client.user?.tag}!`);
});

process.on('beforeExit', () => {
    client.destroy();
});
// If on any individual connection we are the only remaining connection, disconnect
setInterval(() => {
    const connections = client.voice?.connections;
    if (connections) {
        for (const [flake, connection] of Array.from(connections.entries())) {
            if (connection.channel.members.size <= 1) {
                connection.disconnect();
            }
        }
    }
}, 10_000);

// Login
client.login(config.token);
