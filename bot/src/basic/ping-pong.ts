import { Client, Message } from "discord.js";

export const PingPong = (client: Client) => {
    client.on("message", (msg: Message) => {
        if (msg.content === "ping") {
          msg.reply("Pong!");
        }
      });
}