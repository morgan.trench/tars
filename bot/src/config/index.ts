import dotenv from 'dotenv';
import { env } from 'process';

class Configuration {
  
  token: string;
  prefix: string;
  bucket: string;
  
  constructor(){
    dotenv.config()
    this.token = this.ensure('token')
    this.prefix = this.ensure('prefix')
    this.bucket = this.ensure('bucket')
  }

  private ensure(envVar: string):string {
    const val = env[envVar]
    if (val === null || val === undefined){
      throw new Error(`Required environment variable: '${envVar}'`)
    }
    return val
  }

}

export const config = new Configuration()