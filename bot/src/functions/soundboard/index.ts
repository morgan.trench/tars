import { Client, Message, MessageEmbed } from 'discord.js';
import { S3, config as AWSConfig } from 'aws-sdk';
import { config } from '../../config';
import path from 'path';

import NodeCache from 'node-cache';

AWSConfig.apiVersions = {
    s3: '2006-03-01',
};

const Bucket = config.bucket;
const s3 = new S3();

const playMatcher = new RegExp(`^${config.prefix}play (.*)`);
const listMatcher = new RegExp(`^${config.prefix}list ?(.*)`);
const pageSize = 20;

const isString = (str: any): str is string => typeof str === 'string';

const searchSounds = (() => {
    const searchCache = new NodeCache();
    return async (guildId: string, prefix: string, clean: boolean): Promise<string[]> => {
        const key = `${guildId}/${prefix}`;
        const result =
            (searchCache.get(key) as string[]) ||
            (await s3
                .listObjectsV2({ Bucket, Prefix: key })
                .promise()
                .then((x) =>
                    x.Contents?.map((obj) => obj.Key)
                        .filter((Key) => Key && Key[Key.length - 1] !== '/')
                        .filter((Key) => Key && Key.split('/').length == 2)
                        .map((Key) => (clean ? Key && path.parse(Key).name : Key)),
                ));
        searchCache.set(key, result, 5 * 60); // Keep cached for 5 minutes
        return result;
    };
})();

const generateListEmbedPage = (index: number, prefix: string | undefined, soundList: string[]): MessageEmbed => {
    const startIndex = index * pageSize;
    const endIndex = startIndex + pageSize;
    const pageSounds = soundList.slice(startIndex, endIndex).join('\n');
    return new MessageEmbed()
        .setTitle('Sound Listing' + (prefix != '' ? `(${prefix})` : ''))
        .addField(`Page ${index + 1}`, pageSounds, true);
};

export const SoundBoard = (client: Client) => {
    client.on('message', async (msg: Message) => {
        if (client.user?.id === msg.author.id) return;
        const guildId = msg.guild?.id;
        const matchResult = playMatcher.exec(msg.content);
        const terms = matchResult != null && matchResult[1];
        if (terms && msg.member?.voice.channel?.speakable && guildId != null) {
            const soundSequence = await Promise.all(
                terms.split(' ').map((term) =>
                    searchSounds(guildId, term, false)
                        .then((sounds) => sounds.filter((x) => x.length > 0))
                        .then((x) => x[Math.floor(Math.random() * x.length)]),
                ),
            );

            for (const sound of soundSequence) {
                await new Promise(async (res, rej) => {
                    const Key = sound;
                    const voiceConnection = await msg.member?.voice.channel?.join();
                    const soundStream = s3.getObject({ Bucket, Key }).createReadStream();
                    voiceConnection?.play(soundStream).on('finish', res);
                });
            }
        }
    });

    const reactions = ['⬅️', '➡️'];
    const [leftArrow, rightArrow] = reactions;
    client.on('message', async (msg: Message) => {
        if (client.user?.id === msg.author.id) return;
        const guildId = msg.guild?.id;
        const matchResult = listMatcher.exec(msg.content);
        if (matchResult != null && guildId != null) {
            const prefix = matchResult[1];
            const results = await searchSounds(guildId, prefix, true);
            if (results && results.length > 0) {
                let index = 0;
                const reply = await msg.channel.send(generateListEmbedPage(index, prefix, results));
                const resetReactions = async () => {
                    try {
                        await reply.reactions.removeAll();
                        if (index > 0) {
                            await reply.react(leftArrow);
                        }
                        if (index < Math.floor(results.length / pageSize)) {
                            await reply.react(rightArrow);
                        }
                    } catch (error) {
                        msg.channel.send('An error occurred, you are likely missing the "Manage Messages" permission for the bot\n to allow for list navigation, assign this bot a role with the permission')
                        console.log(error);
                    }
                };
                await resetReactions();
                const collector = reply.createReactionCollector((reaction) => reactions.includes(reaction.emoji.name), {
                    time: 60 * 1000,
                });
                collector.on('collect', async (collected) => {
                    if (collected.me && collected.count && collected.count > 1) {
                        if (collected.emoji.name == leftArrow) {
                            index = index > 0 ? index - 1 : 0; // Go Back
                        } else if (collected.emoji.name == rightArrow) {
                            index = index === Math.floor(results.length / pageSize) ? index : index + 1; // Go Forward
                        }
                        await reply.edit(generateListEmbedPage(index, prefix, results));
                        await resetReactions();
                    }
                });
                collector.on('end', () => reply.reactions.removeAll());
            } else {
                msg.reply("sorry but I didn't find anything");
            }
        }
    });
};
