import { Client, VoiceState } from 'discord.js';
import { S3, config as AWSConfig } from 'aws-sdk';
import * as path from 'path';

AWSConfig.apiVersions = {
    s3: '2006-03-01',
};

const Bucket = process.env.bucket;

export const Greetings = (client: Client) => {
    if (Bucket) {
        const s3 = new S3();
        client.on('voiceStateUpdate', async (prevState: VoiceState, state: VoiceState) => {
            if (state.channelID && prevState.channelID != state.channelID) {
                if (state.channel?.speakable) {
                    const voiceConnection = await state.channel?.join();
                    // voiceConnection.play('../resources/obi-wan-hello-there.mp3');
                    voiceConnection.play(s3
                      .getObject({
                          Bucket,
                          Key: 'common/obi-wan-hello-there.mp3',
                      })
                      .createReadStream());
                }
            }
        });
    }
};